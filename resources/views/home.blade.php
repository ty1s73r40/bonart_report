@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                    <a href="{{ route('home') }}" class="list-group-item text-center">Нулиране</a>
            </div>
            <div class="list-group mt-4">
                <div class="list-group-item list-group-item-action">Номер грешка</div>
                <div class="list-group-item list-group-item-action">
                    <form method="POST" action="{{$url}}" id="form_ticket_number">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group" style="margin-bottom: 0px">
                            <input value="{{ (isset($params['ticket_number']))? $params['ticket_number'] : '' }}" onchange="doNumber();" type="number" name="ticket_number" class="form-control" id="ticket_number" placeholder="Въведете номер">
                        </div>
                    </form>
                </div>
                <a onclick="document.getElementById('form_ticket_number').submit();" class="list-group-item text-center">Намери</a>
            </div>
            <div class="list-group mt-4">
                <div class="list-group-item list-group-item-action">ИД грешка</div>
                <div class="list-group-item list-group-item-action">
                    <form method="POST" action="{{$url}}" id="form_error_id">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group" style="margin-bottom: 0px">
                            <input name="error_id" value="{{ (isset($params['error_id']))? $params['error_id'] : '' }}" onchange="doId();" type="number" class="form-control" id="error_id" placeholder="Въведете ИД">
                        </div>
                    </form>
                </div>
                <a onclick="document.getElementById('form_error_id').submit();" class="list-group-item text-center">Намери</a>
            </div>
            <div class="list-group mt-4">
                <div class="list-group-item list-group-item-action">Практика</div>
                @foreach($practices as $practice)
                <a href="{{ ($useGetVariable)? $url.'&' : $url.'?' }}practice_id={{$practice->id}}" class="list-group-item text-center {{ (isset($params['practice_id']) && $params['practice_id'] == $practice->id)? 'active' : '' }}">{{$practice->name}}</a>
                @endforeach
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Резултати от търсенето</div>
                <div class="card-body">
                    <div class="col-12">
                        <table class="table table-hover table-responsive">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">ИД</th>
                                    <th scope="col">Номер</th>
                                    <th scope="col">Грешка</th>
                                    <th scope="col">Преглед</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($errors as $error)
                                <tr>
                                    <th scope="row">{{$error->id}}</th>
                                    <td>{{$error->ticket_number}}</td>
                                    <td>{{$error->message}}</td>
                                    <td>
                                        <a href="{{ route('report', ['id' => $error->id]) }}" class="btn btn-primary">Отвори</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    {{ $errors->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
