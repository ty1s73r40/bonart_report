@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-5">
                <div class="card">
                    <div class="card-header">Информация</div>
                    <div class="card-body">
                    <table class="table table-hover table-responsive">
                        <tbody>
                            <tr>
                                <th class="thead-dark">ИД:</th>
                                <td>{{$error->ticket_number}}</td>
                            </tr>
                            <tr>
                                <th class="thead-dark">Номер:</th>
                                <td>{{$error->ticket_number}}</td>
                            </tr>
                            <tr>
                                <th class="thead-dark">Име на практиката:</th>
                                <td>{{$error->practice->name}}</td>
                            </tr>
                            <tr>
                                <th class="thead-dark">Код на практиката:</th>
                                <td>{{$error->practice->code}}</td>
                            </tr>
                            <tr>
                                <th class="thead-dark">Код на грещката:</th>
                                <td>{{$error->code}}</td>
                            </tr>
                            <tr>
                                <th class="thead-dark">Съобщение:</th>
                                <td>{{$error->message}}</td>
                            </tr>
                            <tr>
                                <th class="thead-dark">Линия:</th>
                                <td>{{$error->line}}</td>
                            </tr>
                            <tr>
                                <th class="thead-dark">Файл:</th>
                                <td>{{$error->file}}</td>
                            </tr>
                            <tr>
                                <th class="thead-dark">Дата:</th>
                                <td>{{$error->date_added}}</td>
                            </tr>
                            <tr>
                                <th class="thead-dark">Тип:</th>
                                <td>{{$error->type}}</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-lg-7">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card mb-3">
                    <div class="card-header">Кодиране</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('save_messages') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="id" value="{{ $message->id }}" />
                            <div class="form-group">
                                <label for="action">Разтълкуване</label>
                                <input value="{{$message->action}}" type="text" class="form-control" id="action" name="action">
                            </div>
                            <button type="submit" class="btn btn-primary">Запис</button>
                        </form>
                    </div>
                </div>
                <div class="card">
                <div class="card-header">XML Редактор:</div>
                <div class="card-body">
                    <textarea id="demo_text"><?= $xml_data ?></textarea>
                </div>
                <div class="card-footer">
                    <a target="_blank" href="{{ route('report-download', ['id' => $error->id]) }}" class="btn btn-outline-success float-right">Сваляне на XML</a>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $(document).ready(function() {
            document.getElementById("demo_text").value = vkbeautify.xml(document.getElementById("demo_text").value);
            CodeMirror.fromTextArea(document.getElementById("demo_text"), {
                mode: 'application/xml',
                lineNumbers: true,
                lineWrapping: false,
                readOnly: false,
                cursorBlinkRate: -1
            });
        });
    </script>
@endsection
