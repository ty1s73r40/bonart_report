@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">Съобщения</div>
                    <div class="card-body">
                        <div class="col-12">
                            <table class="table table-hover  table-responsive">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">ИД</th>
                                    <th scope="col">Грешка</th>
                                    <th scope="col">Тълкуване</th>
                                    <th scope="col">Запис</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($messages as $message)
                                    <tr>
                                        <th scope="row">{{$message->id}}</th>
                                        <td>{{$message->message}}</td>
                                        <td class="w-50">
                                            <form method="POST" action="{{ route('save_messages') }}" id="form_message_{{$message->id}}">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                <input type="hidden" name="id" value="{{ $message->id }}" />
                                                <div class="form-group" style="margin-bottom: 0px">
                                                    <input value="{{$message->action}}" type="text" name="action" class="form-control" id="action_{{$message->id}}">
                                                </div>
                                            </form>
                                        </td>
                                        <td>
                                            <a onclick="document.getElementById('form_message_{{$message->id}}').submit()" class="btn btn-primary">Запази</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        {{ $messages->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
