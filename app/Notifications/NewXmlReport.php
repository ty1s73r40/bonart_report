<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewXmlReport extends Notification
{
    use Queueable;
    public $practice = null;
    public $request = null;

    /**
     * Create a new notification instance.
     *
     * @param $practice
     * @param $request
     */
    public function __construct($practice, $request)
    {
        $this->practice = $practice;
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Bon Art - XML Report')
                    ->subject('Записана нова грешка от XML')
                    ->line('Лечебно заведение: ' . $this->practice->name)
                    ->line('Лечебно заведение РЦЗ: ' . $this->practice->code)
                    ->line('Грешка номер: ' . $this->request->id)
                    ->line('Билет номер: ' . $this->request->ticket_number)
                    ->line('Грешка: ' . $this->request->message)
                    ->action('Преглед', url('/'))
                    ->line('Поздрави Bon Art Report!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toSlack($notifiable)
    {
        $content = ' Bon Art - XML Report ';
        $content .= ' Записана нова грешка от XML ';
        $content .= ' Лечебно заведение: ' . $this->practice->name;
        $content .= ' с РЦЗ: ' . $this->practice->code . ' ';
        $content .= ' Грешка номер: ' . $this->request->id . ' ';
        $content .= ' Билет номер: ' . $this->request->ticket_number . ' ';
        $content .= ' Грешка: ' . $this->request->message;
        return (new SlackMessage)
            ->content($content);
    }
}
