<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Practice extends Model
{
    use SoftDeletes;

    protected $table = 'practices';

    protected $fillable = [
        'code', 'name'
    ];

    public function errors()
    {
        return $this->hasMany('App\Error', 'practice_id', 'id');
    }
}
