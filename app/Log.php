<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
    const TYPE_XML_DATA = 1;
    const TYPE_ERROR_DATA = 2;

    use SoftDeletes;

    protected $table = 'logs';

    protected $fillable = [
        'type', 'request', 'date'
    ];
}
