<?php

namespace App\Http\Resources;

use App\Message;
use Illuminate\Http\Resources\Json\JsonResource;

class ErrorResource extends JsonResource
{
    public function __construct(mixed $resource)
    {
        dump($resource);
        return $this->toArray();
    }

    /**
     * Transform the resource into an array.
     * @return array
     */
    public function toArray()
    {
        dump($this);
        $message = Message::where('message', $this->message)->first();
        if(!$message){
            $message = new Message(['message' => $this->message]);
            $message->save();
        }
        dump($message);
        return [
            'error_id'              => $this->id,
            'error_code'            => $this->code,
            'error_message'         => $this->message,
            'error_line'            => $this->line,
            'error_file'            => $this->file,
            'error_type'            => $this->type,
            'error_date_added'      => $this->date_added,
            'error_ticket_number'   => $this->ticket_number,
            'result_message'        => $message->message,
            'result_action'         => $message->action
        ];
    }
}
