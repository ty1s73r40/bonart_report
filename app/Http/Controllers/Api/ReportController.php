<?php

namespace App\Http\Controllers\Api;
use App\Error;
use App\Log;
use App\Practice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Support\Facades\Storage;

class ReportController extends Controller
{
    public $practice;
    public $error;

    public function xml(Request $request)
    {
        $xml = $request->xml;
        if(!empty($xml)){
            $timeName = strtotime(date("Y-m-d H:i:s")).'.xml';
            Storage::put('xml-data/'.$timeName, $xml, 'public');
            return response()->json(['status' => true, 'filename' => $timeName]);
        }else{
            return response()->json(['status' => false]);
        }
    }

    public function report(Request $request)
    {
        $params = $request->all();
        $practice = Practice::where('code', $params['practice_code'])->first();
        if(!$practice){
            $practice = new Practice();
            $practice->fill(['code' =>  $params['practice_code'], 'name' =>  $params['practice_name']]);
            $practice->save();
        }
        $this->practice = $practice;
        $error = new Error([
            'practice_id'       => $practice->id,
            'code'              => $params['code'],
            'message'           => $params['message'],
            'line'              => $params['line'],
            'file'              => $params['file'],
            'type'              => $params['type'],
            'date_added'        => date("Y-m-d H:i:s"),
            'server_filename'   => $params['server_filename']
        ]);
        $error->save();
        $this->error = $error;
        User::all()->each(function($user) {
            $user->notify(new NewXmlReport($this->practice, $this->error));
        });
        $message = Message::where('message', $error->message)->first();
        if(!$message){
            $message = new Message(['message' => $error->message]);
            $message->save();
        }
        return response()->json([
            'error_id'              => $error->id,
            'error_code'            => $error->code,
            'error_message'         => $error->message,
            'error_line'            => $error->line,
            'error_file'            => $error->file,
            'error_type'            => $error->type,
            'error_date_added'      => $error->date_added,
            'error_ticket_number'   => $error->ticket_number,
            'result_message'        => $message->message,
            'result_action'         => $message->action,
            'telephone'             => '052/ 313 - 398',
        ]);
    }
}
