<?php

namespace App\Http\Controllers;

use App\Error;
use App\Message;
use App\Notifications\NewXmlReport;
use App\Practice;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = $request->fullUrl();
        $useGetVariable = (empty($_GET))? false : true;
        $params = $request->all();
        $practices = Practice::all();
        $errors_query = Error::orderBy('date_added', 'desc');
        foreach ($params as $name=>$value){
            if($name == 'practice_id'){
                $errors_query->where('practice_id', $value);
            }else if($name == 'ticket_number'){
                $errors_query->where('ticket_number', 'like', '%'.$value.'%');
            }else if($name == 'error_id'){
                $errors_query->where('id', $value);
            }
        }
        $errors = $errors_query->paginate(10);
        $errors->withPath($url);
        return view('home', compact('practices', 'params', 'dates', 'url', 'useGetVariable', 'errors'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function messages(Request $request)
    {
        $url = $request->fullUrl();
        $messages = Message::paginate(10);
        $messages->withPath($url);
        return view('messages', compact('messages','url'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save_messages(Request $request)
    {
        $message = Message::find($request->id);
        $message->action = $request->action;
        $message->save();
        return redirect()->back()->with('message', 'Успешна редакция');
    }

    public function report(Request $request, $id)
    {
        $url = $request->fullUrl();
        $error = Error::find($id);
        $message = Message::where('message', $error->message)->first();
        if($file = Storage::get('xml-data/'.$error->server_filename)) {
            $xml = new \SimpleXMLElement($file);
            $xml_data = $xml->asXML();
        }else{
            $xml_data = '';
        }
        return view('report', compact('error','url', 'xml_data', 'message'));
    }

    public function download($id)
    {
        return response()->download(storage_path("app/xml-data/".Error::find($id)->server_filename));
    }
}
