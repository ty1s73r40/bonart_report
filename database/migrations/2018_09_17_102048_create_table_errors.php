<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableErrors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('errors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('practice_id')->nullable()->unsigned();
            $table->foreign('practice_id')->references('id')->on('practices');
            $table->string('code')->nullable();
            $table->text('message')->nullable();
            $table->string('type');
            $table->string('line')->nullable();
            $table->string('file')->nullable();
            $table->string('date_added');
            $table->string('ticket_number');
            $table->string('server_filename')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('errors');
    }
}
