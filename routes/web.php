<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Auth\LoginController@showLoginForm')->name('index');
Route::get('/ticket', 'IndexController@search')->name('ticket');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@index')->name('home');

Route::get('/report/{id}', 'HomeController@report')->name('report');
Route::get('/report/download/{id}', 'HomeController@download')->name('report-download');

Route::get('/messages', 'HomeController@messages')->name('messages');
Route::post('/messages/save', 'HomeController@save_messages')->name('save_messages');

Auth::routes();
Route::post('/password/email', ['as' => 'password.email', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::get('/password/reset', ['as' => 'password.request', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/password/reset', ['as' => '', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::get('/password/reset/{token}', ['as' => 'password.reset','uses' => 'Auth\LoginController@showLoginForm']);
Route::get('/register', ['as' => 'register', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/register', ['as' => '', 'uses' => 'Auth\LoginController@showLoginForm']);
